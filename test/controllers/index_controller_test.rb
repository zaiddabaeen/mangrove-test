require 'test_helper'

class IndexControllerTest < ActionController::TestCase

    test "index is accessible" do
        get :index
        assert_response :success
    end

    test "location is accessible" do
        get :location
        assert_response :success
    end

    test "cities is accessible" do
        post :cities, countryname: 'Netherlands'
        assert_response :success
    end

    test "weather is accessible" do
        post :weather, countryname: "Netherlands", cityname: "Rotterdam"
        assert_response :success
    end
end
