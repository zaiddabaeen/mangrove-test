
class IndexController < ApplicationController
    def index
    end

    def weather

        @client = Savon.client do
            wsdl "http://www.webservicex.com/globalweather.asmx?WSDL"
            endpoint "http://www.webservicex.com/globalweather.asmx"
            log false
        end

        reply = Hash.new
        reply[:weather] = request_weather(params[:cityname], params[:countryname])
        if reply[:weather]
            reply[:success] = 1
            if reply[:weather].is_cold
                reply[:resolution] = "PJs, coffee and the laptop are your friends"
            else
                reply[:resolution] = "Get these shorts on and go for a run!"
            end
        end

        render :json => reply

    end

    def cities

        @client = Savon.client do
            wsdl "http://www.webservicex.com/globalweather.asmx?WSDL"
            endpoint "http://www.webservicex.com/globalweather.asmx"
            log false
        end

        cities = request_cities(params[:countryname])

        cities = cities.join(", ");
        render :text => cities

    end

    def location

        require 'open-uri' # Required for Unit testing
        remote_ip = open('http://whatismyip.akamai.com').read
        geoplugin_reply = open('http://www.geoplugin.net/json.gp?ip=' + remote_ip).read
        json = JSON.parse(geoplugin_reply)
        result = {:city => json["geoplugin_city"], :country => json["geoplugin_countryName"]}

        render :json => result

    end

    # Requests data using SOAP and returns the data in a hash
    def request_weather(cityname, countryname)
        results = {};
        response = @client.call(:get_weather, message: {'CityName' => cityname, 'CountryName' => countryname})

        doc = Nokogiri::XML(response.to_xml)
        node = doc.xpath("//w:GetWeatherResult", "w"=>"http://www.webserviceX.NET").children()
        xml = HTMLEntities.new.decode node.to_s
        weatherxml = Nokogiri::XML(xml)

        if(weatherxml.xpath("//Status").text == "Success")
            weather = Weather.new(weatherxml)
        else
            return false
        end

        return weather
    end

    # Requests data using SOAP and returns the data as an array
    def request_cities(countryname)
        cities = [];
        response = @client.call(:get_cities_by_country, message: {'CountryName' => countryname})

        doc = Nokogiri::XML(response.to_xml)
        node = doc.xpath("//w:GetCitiesByCountryResult", "w"=>"http://www.webserviceX.NET").children()
        xml = HTMLEntities.new.decode node.to_s
        cityxml = Nokogiri::XML(xml)

        cities = [];
        cityxml.search('//Table').each do |entry|
            cities << "#{entry.at('City').text.strip}"
        end

        return cities
    end
end
