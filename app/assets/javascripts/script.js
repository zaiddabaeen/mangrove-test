function getWeather(){

    var cityname = $("#cityname").val();
    var countryname = $("#countryname").val();

    if(cityname.length == 0 || countryname.length == 0){
        return false;
    }

    $.ajax({
        url: 'index/weather',
        data: $("#weatherForm").serialize(),
        type: "get",
        dataType: 'JSON',
        cache: true,
        timeout: 10000,
        beforeSend: function (){
            $("#loader").show();
            $("#error-block").hide();
            $("#data-block").hide();
            $("#cities-block").hide();
        },
        success: function (response){
            $("#loader").hide();

            if(response['success'] == '1'){
                $("#data-block").show();

                var weather = response['weather'];
                $("#location").html(weather['location']);
                $("#temperature").html(weather['temperature']);
                $("#sky").html(weather['sky']);
                $("#wind").html(weather['wind']);
                $("#pressure").html(weather['pressure']);
                $("#visibility").html(weather['visibility']);

                $("#resolution").html(response['resolution']);
            } else {
                $("#error-block").show();
            }
        },
        error: function (err){
            console.log(err)
            $("#error-block").show();
        }
    });
}

function searchCities(){

    $.ajax({
        url: 'index/cities',
        data: $("#weatherForm").serialize(),
        type: "get",
        cache: true,
        timeout: 10000,
        beforeSend: function (){
            $("#loader").show();
            $("#error-block").hide();
            $("#data-block").hide();
            $("#cities-block").hide();
        },
        success: function (response){
            $("#loader").hide();

            if(response.length){
                $("#cities-block").show();
                $("#cities").html(response);
            } else {
                $("#cities-block").show();
                $("#cities").html("None<br />Are you sure you entered a valid country?");
            }
        },
        error: function (err){
            console.log(err);
            $("#error-block").show();
        }
    });
}

function guessLocation(){

    $.ajax({
        url: 'index/location',
        type: "get",
        dataType: 'JSON',
        cache: true,
        timeout: 5000,
        beforeSend: function (){
            $("#loader").show();
            $("#error-block").hide();
            $("#data-block").hide();
            $("#cities-block").hide();
        },
        success: function (response){
            $("#loader").hide();

            $("#cityname").val(response['city']);
            $("#countryname").val(response['country']);

            getWeather();
        },
        error: function (err){
            console.log(err)
            $("#error-block").show();
        }
    });
}
