class Weather

    attr_accessor :location, :temperature, :sky, :wind, :visibility, :humidity, :pressure

    def initialize (weatherxml)
        @location = "#{weatherxml.xpath("//Location").text.strip}"
        @temperature = "#{weatherxml.xpath("//Temperature").text.strip}"
        @sky = "#{weatherxml.xpath("//SkyConditions").text.strip}"
        @wind = "#{weatherxml.xpath("//Wind").text.strip}"
        @visibility = "#{weatherxml.xpath("//Visibility").text.strip}"
        @humidity = "#{weatherxml.xpath("//RelativeHumidity").text.strip}"
        @pressure = "#{weatherxml.xpath("//Pressure").text.strip}"

        format_attributes
    end

    def setWeather (location, temperature, sky, wind, visibility, humidity, pressure)
        @location = location
        @temperature = temperature
        @sky = sky
        @wind = wind
        @visibility = visibility
        @humidity = humidity
        @pressure = pressure

        format_attributes
    end

    def format_attributes

        @visibility = @visibility.remove(":0")
        @wind = @wind.remove(":0")
        if @sky.length == 0
            @sky = "N/A"
        end

    end

    def is_cold
        return temperature.split.first.to_i < 71
    end

end
